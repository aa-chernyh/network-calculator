class Drag {

    //------------------------------Private------------------------------//
    static instance;

    draggableElem = '.draggable';
    clonableElem = '.clonable';
    droppableElem = '.droppable';
    clonablePlacement = '.addItem .items';
    droppablePlacement = '.items.droppable';
    clonableToDroppableOffset;

    //-----------------------------/Private------------------------------//

    static getInstance() {
        if (Drag.instance === undefined) {
            Drag.instance = new Drag();
        }
        return Drag.instance;
    }

    //------------------------------Drag------------------------------//  
    startClone() {
        this.setGrayOnDragOnElem(this.clonableElem);
        this.calculateClonableToDroppableOffset();

        $(this.clonableElem).draggable({
            helper: 'clone'
        });

        $(this.droppableElem).droppable({
            accept: this.clonableElem,
            drop: function () {
                let clone = $('.ui-draggable-dragging').clone().removeClass('clonable ui-draggable ui-draggable-handle ui-draggable-dragging').addClass(Drag.instance.draggableElem.slice(1));
                Drag.instance.setElemOffset(clone, 'left');
                Drag.instance.setElemOffset(clone, 'top');
                $(clone).attr('data-id', BaseElem.getNewElemDataId());
                $(Drag.instance.droppableElem).append(clone);

                BaseElem.refreshEvents(clone);
                BaseElem.initializeElemClass(clone);
            }
        });
    }

    startDragOnElem(elem) {
        $(elem).draggable();
        $(elem).on('drag', function () {
            Arrows.getInstance().redraw();
        });
    }

    setGrayOnDragOnElem(elem) {
        $(elem).on('dragstart', function () {
            $(this).css('filter', 'grayscale(100%)');
        });

        $(elem).on('dragstop', function () {
            $(this).css('filter', '');
        });
    }
    //-----------------------------/Drag------------------------------//

    //------------------------------Helpers------------------------------//
    setElemOffset(elem, position) {
        let currentDistance = elem.css(position).substr(0, elem.css(position).length - 2);
        elem.css(position, parseFloat(currentDistance) + this.clonableToDroppableOffset[position]);
    }

    calculateClonableToDroppableOffset() {
        let clonableOffset = $(this.clonablePlacement).offset();
        let droppableOffset = $(this.droppablePlacement).offset();
        this.clonableToDroppableOffset = {
            'left': clonableOffset.left - droppableOffset.left,
            'top': clonableOffset.top - droppableOffset.top
        };
    }
    //-----------------------------/Helpers------------------------------//
}
