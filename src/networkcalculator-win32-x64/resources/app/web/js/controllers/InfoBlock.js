class InfoBlock {
    static getElemInfoByClick(elem) {
        $(elem).on('click', function () {
            CommonAnimation.disableGrayRoute();
            CommonAnimation.disableGrayText();
            CommonAnimation.setGrayTextForElem(elem);

            //Скрываем все поля
            $('.infoBlock__tabs__content div[data-number=1] tr:not(.hidden)').addClass('hidden');

            //Открываем только те, которые нужны
            let elemParams = BaseElem.getClassByDataId($(elem).data('id')).getParams();
            elemParams.forEach(function (param) {
                let infoBlockTableTr = $('#param_' + BaseElem.getKeyFromParamObj(param));
                $(infoBlockTableTr).find('input').val(BaseElem.getValueFromParamObj(param));
                infoBlockTableTr.removeClass('hidden');
            });

            //Покажем кнопку сохранения
            $('.infoBlock__tabs__content__save input[type=button]').parent().parent().removeClass('hidden');

            //Пропишем data-id элемента
            $('.infoBlock__tabs__content div[data-number=1] input[name=elemDataId]').val($(elem).data('id'));

            //Откроем вкладку, если закрыта
            let tab = $('.infoBlock__tabs__header li[data-number=1]');
            if (!tab.hasClass('active')) {
                CommonAnimation.toggleRightMenuTabs(tab);
            }
        });
    }

    static saveElemParams() {
        $('.infoBlock__tabs__content__save input[type=button]').on('click', function () {
            let elemDataId = $('.infoBlock__tabs__content div[data-number=1] input[name=elemDataId]').val();
            let elemClass = BaseElem.getClassByDataId(InfoBlock.getElemDataId());
            $('.infoBlock__tabs__content div[data-number=1] input[type=text]').each(function () {
                ;
                let paramKey = $(this).parent().parent().attr('id').replace('param_', '');
                elemClass.setParam(paramKey, $(this).val());
            });

            StatusBar.temporarySet('Сохранено');
        });
    }

    static getElemDataId() {
        return $('.infoBlock__tabs__content div[data-number=1] input[name=elemDataId]').val();
    }

    static mapChainElemsInfo(params) {
        //Скрываем все поля
        $('.infoBlock__tabs__content div[data-number=2] tr:not(.hidden)').addClass('hidden');

        //Ищем нужный раздел
        params.forEach(function (section) {
            let sectionId = BaseElem.getKeyFromParamObj(section);
            let sectionParams = BaseElem.getValueFromParamObj(section);
            //Открываем разделители секций
            $('.infoBlock__tabs__content div[data-number=2] hr').removeClass('hidden');
            //Открываем только те параметры, которые нужны
            sectionParams.forEach(function (param) {
                let paramName = BaseElem.getKeyFromParamObj(param);
                let paramValue = BaseElem.getValueFromParamObj(param);

                switch (paramName) {
                    case 'bestRoute':
                        break;
                    case 'bestRoutePrettyView':
                        $('#' + sectionId + ' .tableHeader').removeClass('hidden');
                        $('#' + sectionId + ' .param_bestRoutePrettyViewHeader').removeClass('hidden');
                        $('#' + sectionId + ' .param_bestRoutePrettyView').removeClass('hidden');
                        $('#' + sectionId + ' .param_bestRoutePrettyView > td > span').text(paramValue);
                        break;
                    case 'dataForSpeedChart':
                        $('#' + sectionId + ' .param_speedChart').removeClass('hidden');
                        InfoBlock.startChartMiddleSpeed(JSON.parse(paramValue));
                    default:
                        let infoBlockTableTr = $('#' + sectionId + ' .param_' + paramName);
                        $(infoBlockTableTr).find('input').val(paramValue);
                        infoBlockTableTr.removeClass('hidden');
                        break;
                }
            });
        });

        //Откроем вкладку, если закрыта
        let tab = $('.infoBlock__tabs__header li[data-number=2]');
        if (!tab.hasClass('active')) {
            CommonAnimation.toggleRightMenuTabs(tab);
        }
    }

    static getPrettyViewOfBestRouteNames(routeNames) {
        let result = "";
        routeNames.forEach(function (elemNumber, index) {
            let elemClass = BaseElem.getClassByDataId(BaseElem.getDataIdByNumber(elemNumber));
            result += elemClass.getParam('name');
            if (index != (routeNames.length - 1)) {
                result += " > ";
            }
        });
        return result;
    }

    static changeParamIfChangedParam(changedParam, param, calculateMethod) {
        $(changedParam).on('keyup', function () {
            let calculatedParam = calculateMethod($(changedParam).val());
            $(param).val(BaseElem.getValueFromParamObj(calculatedParam));
        });
    }

    static startChartMiddleSpeed(data) {
        $('.canvasPlacement').text('');
        $('.canvasPlacement').append('<canvas id="middleSpeedChart" height="200" width="410" style="width: 410px; height: 200px;"></canvas>');
        var ctxL = $('#middleSpeedChart')[0].getContext('2d');
        new Chart(ctxL, {
            type: 'line',
            data: {
                labels: Object.keys(data),
                datasets: [{
                        label: "Скорость за час (бит/с)",
                        data: Object.values(data),
                        backgroundColor: [
                            'rgba(74, 72, 66, .7)'
                        ],
                        borderColor: [
                            'rgba(182, 202, 219, .7)'
                        ],
                        borderWidth: 2
                    }]
            },
            options: {
                responsive: false
            }
        });
        Chart.defaults.global.defaultFontColor = 'rgb(182, 202, 219)';
        Chart.defaults.global.defaultFontSize = 14;
    }
}
