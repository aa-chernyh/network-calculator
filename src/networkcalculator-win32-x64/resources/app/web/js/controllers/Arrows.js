class Arrows {
    //------------------------------private------------------------------//
    static instance;

    arrowsLib; //экземпляр библиотеки отрисовки стрелок
    libOptions = {//настройки библиотеки
        arrow: {
            arrowType: 'line'
        }
    }
    elements = []; //массив связей элементов сети
    canvasPlacement = '.content .items'; //блок, в котором распологаются elements
    interactionStateEnum = Object.freeze({//enum состояния взаимодействия элементов
        "stop": "stop",
        "connect": "connect",
        "disconnect": "disconnect",
        "calculate": "calculate"
    });
    interactionState; //текущее состояние взаимодействия

    routes = []; //Маршруты getAllRoutes()

    constructor() {
        this.arrowsLib = $cArrows(this.canvasPlacement, this.libOptions);
        this.interactionState = this.interactionStateEnum.stop;
    }
    //-----------------------------/private------------------------------//

    static getInstance() {
        if (Arrows.instance === undefined) {
            Arrows.instance = new Arrows();
        }
        return Arrows.instance;
    }

    //------------------------------connect------------------------------//
    addArrow(fromNumber, toNumber) {
        let isDublicate = false;
        Arrows.instance.elements.forEach(function (value, index) {
            if (
                    value.elems[0] === BaseElem.getIdByNumber(fromNumber) && value.elems[1] === BaseElem.getIdByNumber(toNumber)
                    || value.elems[1] === BaseElem.getIdByNumber(fromNumber) && value.elems[0] === BaseElem.getIdByNumber(toNumber)
                    ) {
                isDublicate = true;
            }
        });

        if (!isDublicate) {
            let networkParams = []; //сетевые параметры
            //рассчитаем время реакции соединения
            networkParams.push(ReactionTime.getNetworkReactionForArrow());

            let arrow = {
                'elems': Arrows.instance.getArrowElemByNumbers(fromNumber, toNumber),
                'networkParams': networkParams
            };
            Arrows.instance.elements.push(arrow);

            Arrows.instance.arrowsLib.arrow(BaseElem.getIdByNumber(fromNumber), BaseElem.getIdByNumber(toNumber));
        }
    }

    connectStart() {
        Arrows.instance.interactionStart('click.' + Arrows.instance.interactionStateEnum.connect, Arrows.instance.addArrow);
        Arrows.instance.interactionState = Arrows.instance.interactionStateEnum.connect;
    }

    connectStop() {
        Arrows.instance.interactionStop('click.' + Arrows.instance.interactionStateEnum.connect);
        Arrows.instance.interactionState = Arrows.instance.interactionStateEnum.stop;
    }

    connectToggle() {
        this.toggleInteraction(this.interactionStateEnum.connect, this.connectStart, this.connectStop);
    }
    //-----------------------------/connect------------------------------//

    //------------------------------disconnect------------------------------//
    removeArrow(fromNumber, toNumber) {
        Arrows.instance.elements.forEach(function (value, index) {
            if (
                    value.elems[0] === BaseElem.getIdByNumber(fromNumber) && value.elems[1] === BaseElem.getIdByNumber(toNumber)
                    || value.elems[1] === BaseElem.getIdByNumber(fromNumber) && value.elems[0] === BaseElem.getIdByNumber(toNumber)
                    ) {
                Arrows.instance.elements.splice(index, 1); //удаляем элемент
            }
        });
        Arrows.instance.redraw();
    }

    disconnectStart() {
        Arrows.instance.interactionStart('click.' + Arrows.instance.interactionStateEnum.disconnect, Arrows.instance.removeArrow);
        Arrows.instance.interactionState = Arrows.instance.interactionStateEnum.disconnect;
    }

    disconnectStop() {
        $(BaseElem.elemIdentificator).unbind('click.' + Arrows.instance.interactionStateEnum.disconnect);
        Arrows.instance.interactionState = Arrows.instance.interactionStateEnum.stop;
    }

    disconnectToggle() {
        this.toggleInteraction(this.interactionStateEnum.disconnect, this.disconnectStart, this.disconnectStop);
    }

    removeArrowsOnElement(elem) {
        let elemNumber = BaseElem.getNumberByElem(elem);
        let elementsClone = this.elements.slice(); //Клонируем, потому что массив во время цикла изменяется и цикл прерывается
        elementsClone.forEach(function (value, index) {
            if (
                    value.elems[0] === BaseElem.getIdByNumber(elemNumber)
                    || value.elems[1] === BaseElem.getIdByNumber(elemNumber)
                    ) {
                Arrows.instance.removeArrow(BaseElem.getNumberByIdentificator(value.elems[0]), BaseElem.getNumberByIdentificator(value.elems[1]));
            }
        });
    }
    //-----------------------------/disconnect------------------------------//

    //------------------------------calculate------------------------------//
    calculateAction(fromNumber, toNumber) {
        CommonAnimation.showLoader();

        //Выполняем вычисления в отдельном потоке
        setTimeout(function () {
            Arrows.instance.getAllRoutes.routes = [];
            Arrows.instance.getAllRoutes(fromNumber, toNumber);
            if (Arrows.instance.getAllRoutes.routes.length !== 0) {
                let paramsArray = new Array();
                paramsArray.push({'reactionTime': ReactionTime.calculateCommonReactionTime(Arrows.instance.getAllRoutes.routes)});
                paramsArray.push({'unsentPackets': UnsentPackets.calculateAmountOfInformationInUnsentPackets(Arrows.instance.getAllRoutes.routes)});
                paramsArray.push({'performance': Performance.calculateCommonPerformance(Arrows.instance.getAllRoutes.routes)});

                CommonAnimation.isCalculated = true;
                InfoBlock.mapChainElemsInfo(paramsArray);
                Grade.calculate(paramsArray);
            } else {
                StatusBar.temporarySet('Элементы не имеют связей!');
            }

            CommonAnimation.hideLoader();
        }, 1000);
    }

    calculateStart() {
        Arrows.instance.interactionStart('click.' + Arrows.instance.interactionStateEnum.calculate, Arrows.instance.calculateAction);
        Arrows.instance.interactionState = Arrows.instance.interactionStateEnum.calculate;
    }

    calculateStop() {
        Arrows.instance.interactionStop('click.' + Arrows.instance.interactionStateEnum.calculate);
        Arrows.instance.interactionState = Arrows.instance.interactionStateEnum.stop;
    }

    calculateToggle() {
        this.toggleInteraction(this.interactionStateEnum.calculate, this.calculateStart, this.calculateStop);
    }
    //-----------------------------/calculate------------------------------//

    //------------------------------helpers------------------------------//   
    toggleInteraction(interactionType, startMethod, stopMethod) {
        if (
                interactionType !== this.interactionState
                && this.interactionState !== this.interactionStateEnum.stop
                ) {
            this.stopAllInteraction();
        }

        if (this.interactionState === this.interactionStateEnum.stop) {
            startMethod();
        } else {
            stopMethod();
        }
    }

    refreshInteraction() {
        let interactionState = this.interactionState;
        this.stopAllInteraction();
        this.startInteractionByAction(interactionState);
    }

    stopAllInteraction() {
        switch (this.interactionState) {
            case this.interactionStateEnum.connect:
                this.connectStop();
                return;
            case this.interactionStateEnum.disconnect:
                this.disconnectStop();
                return;
            case this.interactionStateEnum.calculate:
                this.calculateStop();
                return;
            default:
                return;
        }
    }

    toggleInteractionByAction(action) {
        switch (action) {
            case this.interactionStateEnum.connect:
                this.connectToggle();
                return;
            case this.interactionStateEnum.disconnect:
                this.disconnectToggle();
                return;
            case this.interactionStateEnum.calculate:
                this.calculateToggle();
                return;
            default:
                return;
        }
    }

    startInteractionByAction(action) {
        switch (action) {
            case this.interactionStateEnum.connect:
                this.connectStart();
                return;
            case this.interactionStateEnum.disconnect:
                this.disconnectStart();
                return;
            case this.interactionStateEnum.calculate:
                this.calculateStart();
                return;
            default:
                return;
        }
    }

    interactionStart(eventName, method) {
        $(BaseElem.elemIdentificator).on(eventName, function () {
            if (Arrows.instance.interactionStart.linkedElem1Number === undefined) {
                Arrows.instance.interactionStart.linkedElem1Number = BaseElem.getNumberByElem(this);
                CommonAnimation.setGrayRoute([Arrows.instance.interactionStart.linkedElem1Number]);
            } else {
                let linkedElem2Id = BaseElem.getNumberByElem(this);
                if (Arrows.instance.interactionStart.linkedElem1Number !== linkedElem2Id) {
                    method(Arrows.instance.interactionStart.linkedElem1Number, linkedElem2Id);
                }
                Arrows.instance.interactionStart.linkedElem1Number = undefined;

                if (eventName !== 'click.' + Arrows.instance.interactionStateEnum.calculate) {
                    CommonAnimation.disableGrayRoute();
                }
            }
        });
    }

    interactionStop(eventName) {
        $(BaseElem.elemIdentificator).unbind(eventName);
    }

    getArrowElemByNumbers(fromId, toId) {
        return [BaseElem.getIdByNumber(fromId), BaseElem.getIdByNumber(toId)];
    }

    redraw() {
        //Удаляем объект-рисователь и канву
        Arrows.instance.arrowsLib.clear();
        delete Arrows.instance.arrowsLib;
        $(Arrows.instance.canvasPlacement + ' > canvas').eq(0).remove();

        //Рисуем новую канву
        let elemsToDraw = [];
        Arrows.instance.elements.forEach(function (value, index) {
            elemsToDraw.push(value.elems);
        });

        Arrows.instance.arrowsLib = $cArrows(Arrows.instance.canvasPlacement, Arrows.instance.libOptions);
        Arrows.instance.arrowsLib.arrows(elemsToDraw);
        Arrows.instance.arrowsLib.draw();
    }

    //Осторожно!!! Рекурсия!!!
    getAllRoutes(fromNumber, toNumber, route = []) {
        route.push(fromNumber);

        if (fromNumber === toNumber) {
            //Если путь известен, записать и выйти
            Arrows.instance.getAllRoutes.routes.push(route);
        } else {
            let allRoutes = Arrows.instance.getAllRoutesForElem(fromNumber);
            let nextRoutes = Arrows.instance.deleteElemsFromArray(route, allRoutes);
            if (nextRoutes.length === 0) {
                return;
            }

            for (let i = 0; i < nextRoutes.length; i++) {
                //Идем по рекурсии
                let routeClone = Object.assign([], route);
                Arrows.instance.getAllRoutes(nextRoutes[i], toNumber, routeClone);
            }
        }

        return Arrows.instance.routes;
    }

    getAllRoutesForElem(elemNumber) {
        let routes = [];

        for (let i = 0; i < Arrows.instance.elements.length; i++) {
            let arrowElem0Number = BaseElem.getNumberByIdentificator(Arrows.instance.elements[i].elems[0]);
            let arrowElem1Number = BaseElem.getNumberByIdentificator(Arrows.instance.elements[i].elems[1]);
            if (Arrows.instance.elements[i].elems[0] === BaseElem.getIdByNumber(elemNumber)) {
                routes.push(arrowElem1Number);
            }
            if (Arrows.instance.elements[i].elems[1] === BaseElem.getIdByNumber(elemNumber)) {
                routes.push(arrowElem0Number);
            }
        }

        return routes;
    }

    deleteElemsFromArray(deleteElements, arr) {
        let result = [];

        for (let i = 0; i < arr.length; i++) {
            if (deleteElements.indexOf(arr[i]) === -1) {
                result.push(arr[i]);
            }
        }

        return result;
    }
    //-----------------------------/helpers------------------------------//   
}
