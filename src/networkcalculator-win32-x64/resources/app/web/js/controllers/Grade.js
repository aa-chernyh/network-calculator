class Grade {
    static ratingPlacement = '.grade__rating > span';
    
    //------------------------------public------------------------------//
    static calculateIfMethodChanged() {
        $('input[type=radio][name=gradeMethod]').on('click', function() {
            if(GradeMethod.parsedParams !== undefined) {
                Grade.mapRating();
            }
        });
        
        $('.grade__method__custom__calculate > input[type=button]').on('click', function() {
            if($('#grade__method__custom').prop("checked") && GradeMethod.parsedParams !== undefined) {
                Grade.mapRating();
            }
        });
    }
    
    static calculate(params) {
        GradeMethod.parseAndSaveParams(params);
        Grade.mapRating();
    }
    //-----------------------------/public------------------------------//
    
    //------------------------------private------------------------------//
    static mapRating() {
        $(Grade.ratingPlacement).text(Grade.getRating().toFixed(CommonParam.decimalPlaces));
    }
    
    static getRating() {
        let selectedRadioButton;
        $('input[type=radio][name=gradeMethod]').each(function() {
            if($(this).prop("checked")) {
                selectedRadioButton = $(this).prop('id');
            }
        });
        
        switch(selectedRadioButton) {
            case 'grade__method__middle':
                return GradeMethod.getMiddle();
            case 'grade__method__median':
                return GradeMethod.getMedian();
            case 'grade__method__max':
                return GradeMethod.getMax();
            case 'grade__method__min':
                return GradeMethod.getMin();
            case 'grade__method__custom':
                return GradeMethod.getCustom();
        }
    }
    //-----------------------------/private------------------------------//
}
