/*
 * Сетевой калькулятор
 * Автор: Александр Черных
 * Почта для связи: AA-Chernyh@mail.ru
 */

$(function () {
    CommonAnimation.toggleAddItemsBlock();
    CommonAnimation.toggleMenu();
    CommonAnimation.toggleInfoBlock();
    CommonAnimation.showStatusBarDescription();
    CommonAnimation.toggleRightMenuTabsOnClick('.infoBlock__tabs__header li');
    CommonAnimation.startToggleCustomGradeMethod();

    Switches.start('.switch-btn[data-action=connect]');
    Switches.start('.switch-btn[data-action=disconnect]');
    Switches.start('.switch-btn[data-action=calculate]');

    InfoBlock.saveElemParams();
    InfoBlock.changeParamIfChangedParam('#param_middleIntervalBetweenTwoPackets input', '#param_averageFlowRate input', UnsentPackets.getAverageFlowRateForElem);
    
    Grade.calculateIfMethodChanged();

    Drag.getInstance().startClone();
    
    Files.openProjectStart();
    Files.downloadProjectStart();
});