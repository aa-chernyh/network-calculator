class BaseElem {
    static iterator = 1;
    static elemList = [];
    static elemTypeEnum = Object.freeze({
        "Hub": "Hub",
        "Network": "Network",
        "PC": "PC",
        "Router": "Router",
        "Switch": "Switch"
    });
    static elemIdentificator = '.content .block'; //идентификатор элемента
    static elemsPlacement = '.content .items'; //местоположение элементов

    constructor(elem, needSetCommonPrams = true) {
        this.elem = elem;
        this.type = elem.data('type');
        if(needSetCommonPrams) {
            this.setCommonParams();
        }
        InfoBlock.getElemInfoByClick(elem);
    }

    setCommonParams() {
        this.setParam('name', this.type + ' ' + BaseElem.getNumberByIdentificator($(this.elem).data('id')));

        //ReactionTime
        this.setParamObj(ReactionTime.getHardwareReactionForElement(this));

        //UnsentPackets
        let middleIntervalBetweenTwoPackets = UnsentPackets.getMiddleIntervalBetweenTwoPacketsForElem(this);
        this.setParamObj(middleIntervalBetweenTwoPackets);
        this.setParamObj(UnsentPackets.getAverageFlowRateForElem(BaseElem.getValueFromParamObj(middleIntervalBetweenTwoPackets)));
        this.setParamObj(UnsentPackets.getPacketGenerationIntensityForElem(this));
        this.setParamObj(UnsentPackets.getPacketSize(this));
        this.setParamObj(UnsentPackets.getInputBufferSize(this));

        //Performance
        this.setParamObj(Performance.getMinSizeOfTransmittedInfo(this));
        this.setParamObj(Performance.getMaxSizeOfTransmittedInfo(this));

        this.mapParams();
    }

    setParamObj(paramObj) {
        this.setParam(BaseElem.getKeyFromParamObj(paramObj), BaseElem.getValueFromParamObj(paramObj));
    }

    setParam(key, value) {
        let paramIsSet = false;
        let params = this.getParams();

        //замена
        params.forEach(function (param, index) {
            if (param[key] !== undefined) {
                param[key] = value;
                paramIsSet = true;
            }
        });

        //добавление
        if (!paramIsSet) {
            params.push(BaseElem.getParamObjectByKeyAndValue(key, value));
        }

        //запись
        $(this.elem).attr('data-params', JSON.stringify(params));

        if (key === 'name') {
            BaseElem.setElemName(this);
        }
    }

    getParams() {
        let params = $(this.elem).data('params');
        if (params === undefined) {
            return [];
        }
        return params;
    }

    getParam(name) {
        let params = this.getParams();
        let value = false;
        params.forEach(function (param, index) {
            if (param[name] !== undefined) {
                value = param[name];
            }
        });
        return value;
    }

    static getParamObjectByKeyAndValue(key, value) {
        let namedArray = [];
        namedArray[key] = value;
        return Object.assign({}, namedArray);
    }

    static getKeyFromParamObj(param) {
        return Object.keys(param)[0];
    }

    static getValueFromParamObj(param) {
        return Object.values(param)[0];
    }

    static setElemName(elemClass) {
        let elemNameBlock = $(elemClass.elem).find('.block__name');
        elemNameBlock.text(elemClass.getParam('name'));
        if (elemNameBlock.hasClass('hidden')) {
            elemNameBlock.removeClass('hidden');
        }
    }

    static getClassByDataId(dataId) {
        let elemClass = false;
        BaseElem.elemList.forEach(function (value, index) {
            if (value.elem.data('id') === dataId) {
                elemClass = value;
            }
        });
        return elemClass;
    }

    static getNumberByElem(elem) {
        return $(elem).data('id').substr(2);
    }

    static getIdByNumber(number) {
        return BaseElem.elemIdentificator + '[data-id=el' + number + ']';
    }

    static getDataIdByNumber(number) {
        return 'el' + number;
    }

    static getNumberByIdentificator(identificator) {
        //Может вызвать проблемы, если в идентификаторе будут другие цифры помимо data-id
        let regexp = /\d{1,}/;
        let elemNumber = regexp.exec(identificator);
        return elemNumber[0];
    }

    static initializeElemClass(elem, needSetPrams = true) {
        let elemClass = false;
        switch (elem.data('type')) {
            case BaseElem.elemTypeEnum.Hub:
                elemClass = new Hub(elem, needSetPrams);
                break;
            case BaseElem.elemTypeEnum.Network:
                elemClass = new Network(elem, needSetPrams);
                break;
            case BaseElem.elemTypeEnum.PC:
                elemClass = new PC(elem, needSetPrams);
                break;
            case BaseElem.elemTypeEnum.Router:
                elemClass = new Router(elem, needSetPrams);
                break;
            case BaseElem.elemTypeEnum.Switch:
                elemClass = new SwitchElem(elem, needSetPrams);
                break;
            default:
                break;
        }
        BaseElem.elemList.push(elemClass);
        BaseElem.setElemName(elemClass);
        return elemClass;
    }

    static removeElemFromList(elemDataId) {
        BaseElem.elemList.forEach(function (value, index) {
            if (value.dataId === elemDataId) {
                BaseElem.elemList.splice(index, 1); //удаляем элемент
            }
        });
    }

    static getNewElemDataId() {
        return 'el' + this.iterator++;
    }

    static refreshEvents(elem) {
        $(elem).children('.description').hide();
        //CommonAnimation.showDescriptionOnElement(elem);

        let dragInstance = Drag.getInstance();
        dragInstance.startDragOnElem(elem);
        dragInstance.setGrayOnDragOnElem(elem);
        this.refreshRemove();
        Arrows.getInstance().refreshInteraction();
    }

    static removeStart() {
        $(BaseElem.elemIdentificator).on('contextmenu.remove', function () {
            if (confirm('Удалить элемент?')) {
                Arrows.getInstance().removeArrowsOnElement(this);
                BaseElem.removeElemFromList($(this).data('id'));
                $(this).remove();
            }
            return false;
        });
    }

    static removeStop() {
        $(BaseElem.elemIdentificator).unbind('contextmenu.remove');
    }

    static refreshRemove() {
        this.removeStop();
        this.removeStart();
    }
}
