class GradeMethod {
    static parsedParams;
    
    //------------------------------public------------------------------//
    static parseAndSaveParams(params){
        GradeMethod.parsedParams = [];
        params.forEach(function (section) {
            BaseElem.getValueFromParamObj(section).forEach(function (param) {
                let paramName = BaseElem.getKeyFromParamObj(param);
                let paramValue = parseFloat(BaseElem.getValueFromParamObj(param));
                switch (paramName) {
                    case 'bestRoute':
                    case 'bestRoutePrettyView':
                    case 'dataForSpeedChart':
                        break;
                    default:
                        GradeMethod.parsedParams.push({
                            'name': paramName,
                            'value': paramValue,
                            'shortName': GradeMethod.getShortParamName(paramName)
                        });
                }
            });
        });
    }
    
    static getMiddle() {
        let sum = 0;
        let count = 0;
        
        GradeMethod.parsedParams.forEach(function (param) {
            sum += parseFloat(param.value);
            count++;
        });
        
        return sum / count;
    }
    
    static getMedian() {
        let values = GradeMethod.getSortedValuesOfParams();
        return values[values.length / 2];
    }
    
    static getMax() {
        let values = GradeMethod.getSortedValuesOfParams();
        return values[values.length - 1];
    }
    
    static getMin() {
        let values = GradeMethod.getSortedValuesOfParams();
        return values[0];
    }
    
    static getCustom() {
        let result = 0;
        let formula = $('.grade__method__custom__formula').val();
        if(formula === '') {
            return result;
        }
        
        GradeMethod.parsedParams.forEach(function(param) {
            formula = formula.replace(param.shortName, param.value);
        });
        
        try {
            result = eval(formula);
            //Намеренно не стал заниматься безопасностью вводимых данных
        } catch(e) {
            console.log(e);
        }     
        return result;
    }
    //-----------------------------/public------------------------------//
    
    //------------------------------private------------------------------//
    static getSortedValuesOfParams() {
        let values = [];
        
        GradeMethod.parsedParams.forEach(function(param) {
            values.push(param.value);
        });
        
        values.sort(function(a, b) {
            return a - b;
        });
        
        return values;
    }
        
    static getShortParamName(paramName) {
        switch(paramName){
            case 'maxSpeed':
                return 'MAXS';
            default:
                return paramName.substr(0, 1).toUpperCase() + GradeMethod.getCapsFromString(paramName);
        }
    }
    
    static getCapsFromString(string) {
        let caps = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        return  string.split('').filter(function(char){
                    return ~caps.indexOf(char);
                }).join("");
    }
    //-----------------------------/private------------------------------//
}