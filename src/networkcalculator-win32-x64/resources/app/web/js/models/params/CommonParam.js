class CommonParam {
    static numberOfRuns = 10; //количество оценочных прогонов
    static decimalPlaces = 2; //Знаков после запятой по умолчанию

    //------------------------------protected------------------------------//
    static getArrayOfRandomInteger(min, max) {
        let result = [];
        for (let i = 0; i < CommonParam.numberOfRuns; i++) {
            result.push(CommonParam.getRandomInteger(min, max));
        }

        return result;
    }

    static getRandomInteger(min, max) {
        return Math.floor(CommonParam.getRandomDecimal(min, max));
    }

    static getArrayOfRandomDecimal(min, max) {
        let result = [];
        for (let i = 0; i < CommonParam.numberOfRuns; i++) {
            result.push(CommonParam.getRandomDecimal(min, max));
        }

        return result;
    }

    static getRandomDecimal(min, max) {
        let rand = parseFloat(min) + Math.random() * (max - min);
        return rand;
    }

    static getMaxOfArray(numArray) {
        return Math.max.apply(null, numArray);
    }

    static getMinOfArray(numArray) {
        return Math.min.apply(null, numArray);
    }

    static getRouteElemNames(route) {
        let result = [];

        route.forEach(function (elemNumber) {
            let elemDataId = BaseElem.getDataIdByNumber(elemNumber);
            let elemClass = BaseElem.getClassByDataId(elemDataId);
            result.push(elemClass.name);
        });

        return result;
    }
    //-----------------------------/protected------------------------------//
}