/*
 * Рассчитывает время реации между узлами
 */

class ReactionTime extends CommonParam {
    static maxNetworkReactionTime = 0.030; //секунд
    static minNetworkReactionTime = 0.001; //секунд

    //------------------------------public------------------------------//
    static getNetworkReactionForArrow() {
        let randomReactionTimes = ReactionTime.getArrayOfRandomDecimal(ReactionTime.minNetworkReactionTime, ReactionTime.maxNetworkReactionTime);
        let quantile75Percent = (ReactionTime.getQuantile(randomReactionTimes, 75)).toFixed(ReactionTime.decimalPlaces);
        let quantile25Percent = (ReactionTime.getQuantile(randomReactionTimes, 25)).toFixed(ReactionTime.decimalPlaces);
        let networkReactionTime = (ReactionTime.getNetworkReactionTime(quantile75Percent, quantile25Percent)).toFixed(ReactionTime.decimalPlaces);

        return {
            'reaction': {
                'quantile75Persent': quantile75Percent,
                'quantile25Percent': quantile25Percent,
                'networkReactionTime': networkReactionTime
            }
        };
    }

    static getHardwareReactionForElement(elemClass) {
        return {
            'hardwareReactionTime': (ReactionTime.getRandomDecimal(elemClass.getMinHardwareReactionTime(), elemClass.getMaxHardwareReactionTime())).toFixed(ReactionTime.decimalPlaces)
        };
    }

    static calculateCommonReactionTime(routes) {
        let bestReactionTime = 0;
        let hardwareReactionTimeForBestRoute = 0;
        let networkReactionTimeForBestRoute = 0;
        let bestRoute;

        for (let i = 0; i < routes.length; i++) {
            let hardwareReactionTimeForRoute = (ReactionTime.calculateHardwareReactionTimeForRoute(routes[i])).toFixed(UnsentPackets.decimalPlaces);
            let networkReactionTimeForRoute = (ReactionTime.calculateNetworkReactionTimeForRoute(routes[i])).toFixed(UnsentPackets.decimalPlaces);
            let reactionTimeForRoute = (parseFloat(hardwareReactionTimeForRoute) + parseFloat(networkReactionTimeForRoute)).toFixed(UnsentPackets.decimalPlaces);

            if (reactionTimeForRoute < bestReactionTime || bestReactionTime === 0) {
                bestRoute = routes[i];
                bestReactionTime = reactionTimeForRoute;
                hardwareReactionTimeForBestRoute = hardwareReactionTimeForRoute;
                networkReactionTimeForBestRoute = networkReactionTimeForRoute;
            }
        }

//        CommonAnimation.setGrayRoute(bestRoute);
        return [
            {'commonReactionTime': bestReactionTime},
            {'commonHardwareReactionTime': hardwareReactionTimeForBestRoute},
            {'commonNetworkReactionTime': networkReactionTimeForBestRoute},
            {'bestRoute': bestRoute},
            {'bestRoutePrettyView': InfoBlock.getPrettyViewOfBestRouteNames(bestRoute)}
        ];
    }
    //-----------------------------/public------------------------------//

    //------------------------------private------------------------------//
    static calculateHardwareReactionTimeForRoute(route) {
        let allHardwareReactionTime = 0;
        route.forEach(function (elemNumber) {
            let elemDataId = BaseElem.getDataIdByNumber(elemNumber);
            let elemClass = BaseElem.getClassByDataId(elemDataId);
            let elemHardwareReactionTime = elemClass.getParam('hardwareReactionTime');

            allHardwareReactionTime += parseFloat(elemHardwareReactionTime, 10);
        });
        return allHardwareReactionTime;
    }

    static calculateNetworkReactionTimeForRoute(route) {
        let allNetworkReactionTime = 0;
        for (let i = 0; i < route.length; i++) {
            if ((i + 1) !== route.length) {
                for (let j = 0; j < Arrows.instance.elements.length; j++) {
                    if (
                            BaseElem.getNumberByIdentificator(Arrows.instance.elements[j].elems[0]) == route[i]
                            && BaseElem.getNumberByIdentificator(Arrows.instance.elements[j].elems[1]) == route[i + 1]
                            || BaseElem.getNumberByIdentificator(Arrows.instance.elements[j].elems[1]) == route[i]
                            && BaseElem.getNumberByIdentificator(Arrows.instance.elements[j].elems[0]) == route[i + 1]
                            ) {
                        for (let k = 0; k < Arrows.instance.elements[j].networkParams.length; k++) {
                            if (BaseElem.getKeyFromParamObj(Arrows.instance.elements[j].networkParams[k]) == 'reaction') {
                                allNetworkReactionTime += parseFloat(Arrows.instance.elements[j].networkParams[k].reaction.networkReactionTime, 10);
                            }
                        }
                    }
                }
            }
        }

        return allNetworkReactionTime;
    }

    static getNetworkReactionTime(quantile75Percent, quantile25Percent) {
        return quantile75Percent - quantile25Percent;
    }

    static getQuantile(randomReactionTimes, percent) {
        let maxValue = ReactionTime.getMaxOfArray(randomReactionTimes);
        return maxValue * percent / 100;
    }
    //-----------------------------/private------------------------------//
}
