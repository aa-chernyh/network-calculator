/* 
 * Рассчитывает производительность сети
 */

class Performance extends CommonParam {
    static measurementPeriod = 3600; //секунд (один час)
    static conversionRatePerMinuteForMeasurementPeriod = 60; //коэффециент перевода measurementPeriod в минуты

    //------------------------------public------------------------------//

    //минимальное количество передаваемых данных в секунду
    static getMinSizeOfTransmittedInfo(elemClass) { //бит/с
        return {
            'minSizeOfTransmittedInfo': elemClass.getMinSizeOfTransmittedInfo()
        };
    }

    //максимальное количество передаваемых данных в секунду
    static getMaxSizeOfTransmittedInfo(elemClass) { //бит/с
        return {
            'maxSizeOfTransmittedInfo': elemClass.getMaxSizeOfTransmittedInfo()
        };
    }

    //общий рассчет
    static calculateCommonPerformance(routes) {
        let bestMiddleSpeed = 0;
        let bestInstantSpeed = 0;
        let bestMaxSpeed = 0;
        let bestRippleFactor = 0;
        let bestStandartDeviation = 0;
        let bestDataForSpeedChart = [];
        let bestVariationFactor;
        let bestRoute;

        for (let i = 0; i < routes.length; i++) {
            let arrayOfInstantSpeed = Performance.getArrayOfInstantSpeed(routes[i]);
            let middleSpeed = Performance.getMiddleSpeed(arrayOfInstantSpeed);

            if (middleSpeed > bestMiddleSpeed || bestMiddleSpeed === 0) {
                bestRoute = routes[i];
                bestMiddleSpeed = middleSpeed;
                bestInstantSpeed = Performance.getInstantSpeed(routes[i]);
                bestMaxSpeed = Performance.getMaxSpeed(routes[i]);
                bestRippleFactor = Performance.getRippleFactor(bestMaxSpeed, bestMiddleSpeed);
                bestStandartDeviation = Performance.getStandartDeviation(bestMiddleSpeed, arrayOfInstantSpeed);
                bestDataForSpeedChart = Performance.getDataForSpeedChart(arrayOfInstantSpeed);
                bestVariationFactor = Performance.getVariationFactor(bestStandartDeviation, bestMiddleSpeed);
            }
        }

        //CommonAnimation.setGrayRoute(bestRoute);
        return [
            {'middleSpeed': bestMiddleSpeed.toFixed(Performance.decimalPlaces)},
            {'instantSpeed': bestInstantSpeed},
            {'maxSpeed': bestMaxSpeed},
            {'rippleFactor': bestRippleFactor.toFixed(Performance.decimalPlaces)},
            {'standartDeviation': bestStandartDeviation.toFixed(Performance.decimalPlaces)},
            {'dataForSpeedChart': bestDataForSpeedChart},
            {'variationFactor': bestVariationFactor.toFixed(Performance.decimalPlaces)},
            {'bestRoute': bestRoute},
            {'bestRoutePrettyView': InfoBlock.getPrettyViewOfBestRouteNames(bestRoute)}
        ];
    }
    //-----------------------------/public------------------------------//

    //------------------------------private------------------------------//
    static getArrayOfInstantSpeed(route) { //бит/с
        //эмулируем замер мгновенной скорости в течение часа
        let arrayOfinstantSpeed = [];
        for (let i = 0; i < Performance.measurementPeriod; i++) {
            arrayOfinstantSpeed.push(parseInt(Performance.getInstantSpeed(route)));
        }
        return arrayOfinstantSpeed;
    }

    //средняя скорость
    static getMiddleSpeed(arrayOfInstantSpeed) { //бит/с
        let commonSpeed = 0;
        for (let i = 0; i < Performance.measurementPeriod; i++) {
            commonSpeed += arrayOfInstantSpeed[i];
        }
        return commonSpeed / Performance.measurementPeriod;
    }

    //мгновенная скорость
    static getInstantSpeed(route) { //бит/с
        let middleSpeedOnElems = [];
        route.forEach(function (elemNumber) {
            let elemClass = BaseElem.getClassByDataId(BaseElem.getDataIdByNumber(elemNumber));
            let minSizeOfTransmittedInfo = elemClass.getParam('minSizeOfTransmittedInfo');
            let maxSizeOfTransmittedInfo = elemClass.getParam('maxSizeOfTransmittedInfo');
            middleSpeedOnElems.push(Performance.getRandomInteger(minSizeOfTransmittedInfo, maxSizeOfTransmittedInfo));
        });
        return Performance.getMinOfArray(middleSpeedOnElems);
    }

    //максимальная скорость
    static getMaxSpeed(route) {
        let maxSpeedOnElems = [];
        route.forEach(function (elemNumber) {
            let elemClass = BaseElem.getClassByDataId(BaseElem.getDataIdByNumber(elemNumber));
            let maxSizeOfTransmittedInfo = elemClass.getParam('maxSizeOfTransmittedInfo');
            maxSpeedOnElems.push(maxSizeOfTransmittedInfo);
        });
        return Performance.getMinOfArray(maxSpeedOnElems);
    }

    //коэффициент пульсации
    static getRippleFactor(maxSpeed, middleSpeed) {
        return maxSpeed / middleSpeed;
    }

    //стандартное отклонение
    static getStandartDeviation(middleSpeed, arrayOfInstantSpeed) {
        let sum = 0;
        for (let i = 0; i < Performance.measurementPeriod; i++) {
            sum += Math.pow(arrayOfInstantSpeed[i] - middleSpeed, 2) / (Performance.measurementPeriod - 1);
        }
        return Math.pow(sum, 0.5);
    }

    //данные для построения графика отклонения скорости
    static getDataForSpeedChart(arrayOfInstantSpeed) {
        let numberOfMeasurementPeriods = 13;
        let incrementStep = Performance.measurementPeriod / (numberOfMeasurementPeriods - 1);

        let result = {};
        for (let i = 0; i < Performance.measurementPeriod; i = i + incrementStep) {
            result[(i / Performance.conversionRatePerMinuteForMeasurementPeriod) + ' мин'] = arrayOfInstantSpeed[i];
        }
        result[(Performance.measurementPeriod / Performance.conversionRatePerMinuteForMeasurementPeriod) + ' мин'] = arrayOfInstantSpeed[Performance.measurementPeriod - 1];
        return JSON.stringify(result);
    }

    //коэффициент вариации
    static getVariationFactor(standartDeviation, middleSpeed) {
        return standartDeviation / middleSpeed;
    }
    //-----------------------------/private------------------------------//
}