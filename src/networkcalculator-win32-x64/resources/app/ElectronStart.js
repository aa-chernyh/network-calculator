const path = require('path');
const url = require('url');
const {app, BrowserWindow} = require('electron');

let mainWindow;

function createWindow() {
    mainWindow = new BrowserWindow({
        width: 1280,
        height: 640,
        icon: __dirname + '/web/img/icon.png'
    });

    mainWindow.loadURL(
        url.format({
            pathname: path.join(__dirname, 'web/index.html'),
            protocol: 'file:',
            slashes: true
        })
    );

//    mainWindow.webContents.openDevTools();
    mainWindow.setMenu(null);

    mainWindow.on('closed', function () {
        mainWindow = null;
    });
}

app.on('ready', createWindow);
app.on('mainWindowdow-all-closed', function () {
    app.quit();
});
