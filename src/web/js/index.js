/*
 * Сетевой калькулятор
 * Автор: Александр Черных
 * Почта для связи: AA-Chernyh@mail.ru
 */

$(function () {
    CommonAnimation.toggleAddItemsBlock();
    CommonAnimation.toggleMenu();
    CommonAnimation.toggleInfoBlock();
    CommonAnimation.showStatusBarDescription();

    Switches.start('.switch-btn[data-action=connect]');
    Switches.start('.switch-btn[data-action=disconnect]');
    Switches.start('.switch-btn[data-action=calculate]');

    InfoBlock.toggleTabsOnClick('.infoBlock__tabs__header li');
    InfoBlock.saveElemParams();
    InfoBlock.changeParamIfChangedParam('#param_middleIntervalBetweenTwoPackets input', '#param_averageFlowRate input', UnsentPackets.getAverageFlowRateForElem);

    Drag.getInstance().startClone();
    
    Files.openProjectStart();
    Files.downloadProjectStart();
});