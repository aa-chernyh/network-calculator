class Files {
    static projectFileName = "Network Calculator Project.ncp";
    static fileReader;
    static changeTrigger = false;

    //------------------------------public------------------------------//
    static downloadProjectStart() {
        $('.menu__list__elem__button input[data-action=download]').on('click', function() {
            if($(BaseElem.elemIdentificator).length > 0) {
                Files.download(Files.projectFileName, Files.generateProjectContent());
            } else {
                StatusBar.temporarySet('Нет элементов для сохранения!');
            }
        });
    }
    
    static openProjectStart() {
        $('.menu__list__elem__button input[data-action=open]').on('click', function() {
            let input = $('.menu__list__elem__button input[type=file]');
            input.trigger('click');
            input.on('change', Files.setFileDataToProject);
        });
    }
    //-----------------------------/public------------------------------//

    //------------------------------private------------------------------//
    static download(filename, text) {
        let element = document.createElement('a');
        element.style.display = 'none';
        element.setAttribute('href', 'data:text/plain;charset=utf-8,' + encodeURIComponent(text));
        element.setAttribute('download', filename);
        document.body.appendChild(element);

        element.click();
        document.body.removeChild(element);
    }
    
    static generateProjectContent() {
        return JSON.stringify({
            'content': $(BaseElem.elemsPlacement).html(),
            'arrows': Arrows.instance.elements,
            'elemIterator': BaseElem.iterator
        }, null, 4);
    }
    
    static setFileDataToProject() {
        //Событие change на файле срабатывает несколько раз, поэтому здесь костыль
        if(!Files.changeTrigger) {
            Files.changeTrigger = true;
            
            let inputFiles = $('#openProjectFile').prop('files');
            if(inputFiles.length !== 0) {
                let answerResult = true;
                if(BaseElem.elemList.length !== 0) {
                    answerResult = confirm('Вы действительно хотите стереть начатый проект?');
                }
                if(answerResult) {
                    let file = inputFiles[0];
                    Files.fileReader = new FileReader();
                    //Считывание происходит в отдельном потоке, поэтому fileReader.onload выполняется позже, чем fileReader.readAsText
                    Files.fileReader.onload = Files.mapParams;
                    Files.fileReader.readAsText(file);
                }
            }
        }
    }
    
    static mapParams() {
        let fileParams = JSON.parse(Files.fileReader.result);
        $(BaseElem.elemsPlacement).html(fileParams.content);
        Arrows.getInstance().elements = fileParams.arrows;
        BaseElem.iterator = fileParams.elemIterator;
        
        $(BaseElem.elemIdentificator).each(function () {
            BaseElem.initializeElemClass($(this), false);
            Arrows.getInstance().redraw();
            BaseElem.refreshEvents($(this));
        });
        
        //сбрасываем файл, чтобы можно было повторно прочитать тот же
        $('#openProjectFile').val('');
        Files.changeTrigger = false;
    }
    //-----------------------------/private------------------------------//
}