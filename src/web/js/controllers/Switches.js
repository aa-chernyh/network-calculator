class Switches {
    static start(switchElem) {
        $(switchElem).parent().on('click', function () {
            Switches.toggleSwitch(switchElem);
            Arrows.getInstance().toggleInteractionByAction($(switchElem).data('action'));
        });
    }

    static toggleSwitch(elem) {
        if ($(elem).hasClass('switch-on')) {
            Switches.setSwitchOff(elem);
        } else {
            Switches.setSwitchOn(elem);
        }
    }

    static setSwitchOn(elem) {
        let activeSwitch = $('.menu__list .switch-on');
        if (activeSwitch !== undefined) {
            Switches.setSwitchOff(activeSwitch);
        }

        $(elem).removeClass('switch-off').addClass('switch-on');
        let statusText = $(elem).parent().children('.statusText').text();
        StatusBar.set(statusText);
    }

    static setSwitchOff(elem) {
        $(elem).removeClass('switch-on').addClass('switch-off');
        StatusBar.clear();
    }
}
