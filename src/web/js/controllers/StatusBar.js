class StatusBar {
    static textPlacement = '.header__inner__status';
    static temporaryPeriod = 4000; //миллисекунд
    static actionTaken = false;

    static set(text) {
        StatusBar.clear();
        $(StatusBar.textPlacement).text(text);
    }

    static clear() {
        $(StatusBar.textPlacement).text('');
        StatusBar.actionTaken = true;
    }

    static temporarySet(text) {
        let currentText = $(StatusBar.textPlacement).text();
        StatusBar.set(text);
        StatusBar.actionTaken = false;
        setTimeout(function () {
            if (!StatusBar.actionTaken) {
                StatusBar.set(currentText);
            }
        }, StatusBar.temporaryPeriod);

    }
}
