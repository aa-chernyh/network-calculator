class CommonAnimation {
    
    //------------------------------public------------------------------//
    static showDescriptionOnElement(elem) {
        $(elem).on('mouseover mouseout', function () {
            $(this).children('.description').toggle();
        });
    }

    static toggleAddItemsBlock() {
        $('.addItem .menu__list__elem__action').on('click', function () {
            let arrowDown = $('.addItem .fa-chevron-down');
            let items = $('.addItem .items');
            if ($(arrowDown).length !== 0) {
                arrowDown.removeClass('fa-chevron-down').addClass('fa-chevron-up');
                $(items).slideDown(400);
            } else {
                $('.addItem .fa-chevron-up').removeClass('fa-chevron-up').addClass('fa-chevron-down');
                $(items).slideUp(400);
            }
        });
    }

    static setGrayRoute(route) {
        route.forEach(function (elemNumber) {
            let elem = $(BaseElem.getIdByNumber(elemNumber));
            elem.children('.block__image').css('filter', 'grayscale(100%)');
        });
    }

    static disableGrayRoute() {
        $(BaseElem.elemIdentificator).children('.block__image').css('filter', '');
    }

    static setGrayTextForElem(elem) {
        $(elem).children('.block__name').css('font-weight', 'bold');
    }

    static disableGrayText() {
        $(BaseElem.elemIdentificator).children('.block__name').css('font-weight', 'inherit');
    }

    static showLoader() {
        $('.commonWrapper').css('opacity', '0.5');
        $('.loader').removeClass('hidden');
    }

    static hideLoader() {
        $('.commonWrapper').css('opacity', '1');
        $('.loader').addClass('hidden');
    }
    
    static toggleMenu() {
        $('.menu__slider').on('click', function() {
            $('.wrapper__menu').toggle("slide", {direction: "left"}, 400);
            if($(this).children('i').hasClass('fa-angle-double-left')) {
                CommonAnimation.toggleMenuSlider('hide');
            } else {
                CommonAnimation.toggleMenuSlider('open');
            }
        });
    }

    static toggleInfoBlock() {
        $('.infoBlock__slider').on('click', function() {
            $('.wrapper__infoBlock').toggle("slide", {direction: "right"}, 400);
            if($(this).children('i').hasClass('fa-angle-double-left')) {
                CommonAnimation.toggleInfoBlockSlider('hide');
            } else {
                CommonAnimation.toggleInfoBlockSlider('open');
            }
        });
    }

    static showStatusBarDescription() {
        $('.statusBarDescription').removeClass('hidden');
        StatusBar.set('Перенесите элементы на страницу или загрузите файл, чтобы начать');
        
        $('.statusBarDescription input').on('click', function() {
            $('.statusBarDescription').addClass('hidden');
        });
    }
    //------------------------------public------------------------------//
    
    //------------------------------private------------------------------//
    static toggleMenuSlider(mode) {
        let slider = $('.menu__slider');
        slider.hide();
        slider.children('i').removeClass('fa-angle-double-' + (mode === 'open' ? 'right' : 'left')).addClass('fa-angle-double-' + (mode === 'open' ? 'left' : 'right'));
        slider.css('left', (mode === 'open' ? '198' : '0') + 'px');
        slider.css('background-color', (mode === 'open' ? 'inherit' : '#242C39'));
        setTimeout(function() {
            slider.show();
        }, 400);
    }
    
    static toggleInfoBlockSlider(mode) {
        let slider = $('.infoBlock__slider');
        slider.hide();
        slider.children('i').removeClass('fa-angle-double-' + (mode === 'open' ? 'right' : 'left')).addClass('fa-angle-double-' + (mode === 'open' ? 'left' : 'right'));
        slider.css('right', (mode === 'open' ? '0' : '378') + 'px');
        slider.css('background-color', (mode === 'open' ? '#242C39' : 'inherit'));
        setTimeout(function() {
            slider.show();
        }, 400);
    }
    //-----------------------------/private------------------------------//
}

