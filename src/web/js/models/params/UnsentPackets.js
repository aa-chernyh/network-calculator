/* 
 * Рассчитывает количество информации в необслужанных пакетах
 */

class UnsentPackets extends CommonParam {   

    //------------------------------public------------------------------//
    //средний интервал между двумя соседними пакетами
    static getMiddleIntervalBetweenTwoPacketsForElem(elemClass) { //секунд
        let randomIntervalArray = UnsentPackets.getArrayOfRandomDecimal(elemClass.getMinIntervalBetweenTwoPackets(), elemClass.getMaxIntervalBetweenTwoPackets());

        return {
            'middleIntervalBetweenTwoPackets': ((UnsentPackets.getMaxOfArray(randomIntervalArray) + UnsentPackets.getMinOfArray(randomIntervalArray)) / 2).toFixed(UnsentPackets.decimalPlaces)
        };
    }

    //средняя интенсивность выходного потока
    static getAverageFlowRateForElem(middleIntervalBetweenTwoPackets) { // 1/секунду
        return {
            'averageFlowRate': (1 / middleIntervalBetweenTwoPackets).toFixed(UnsentPackets.decimalPlaces)
        };
    }

    //интенсивность генерации пакетов
    static getPacketGenerationIntensityForElem(elemClass) { //пакетов в секунду
        return {
            'packetGenerationIntensity': Math.round(UnsentPackets.getRandomDecimal(elemClass.getMinPacketGenerationIntensity(), elemClass.getMaxPacketGenerationIntensity()))
        };
    }

    //размер передаваемого пакета
    static getPacketSize(elemClass) { //байт
        return {
            'packetSize': UnsentPackets.getRandomInteger(elemClass.getMinPacketSize(), elemClass.getMaxPacketSize())
        };
    }

    //величина входного буфера
    static getInputBufferSize(elemClass) { //байт
        return {
            'inputBufferSize': UnsentPackets.getRandomInteger(elemClass.getMinInputBufferSize(), elemClass.getMaxInputBufferSize())
        };
    }

    static calculateAmountOfInformationInUnsentPackets(routes) {
        let totalTrafficLoad = 0;
        let averagePacketsInBuffer = 0;
        let probabilityOfPacketLoss = 0;
        let countOfLostPacketsInOneMillisecond = 0;
        let lostSize = 0;
        let bestRoute;

        routes.forEach(function (route) {
            let totalTrafficLoadForRoute = UnsentPackets.getTotalTrafficLoad(route);
            let averagePacketsInBufferForRoute = UnsentPackets.getAveragePacketsInBuffer(route, totalTrafficLoadForRoute);
            let probabilityOfPacketLossForRoute = UnsentPackets.getProbabilityOfPacketLoss(totalTrafficLoadForRoute, averagePacketsInBufferForRoute);
            let countOfLostPacketsInOneMillisecondForRoute = UnsentPackets.getCountOfLostPacketsInOneMillisecond(probabilityOfPacketLossForRoute, route);
            let lostSizeForRoute = UnsentPackets.getLostSize(countOfLostPacketsInOneMillisecondForRoute, route);

            if (lostSizeForRoute < lostSize || lostSize === 0) {
                totalTrafficLoad = totalTrafficLoadForRoute;
                averagePacketsInBuffer = averagePacketsInBufferForRoute;
                probabilityOfPacketLoss = probabilityOfPacketLossForRoute;
                countOfLostPacketsInOneMillisecond = countOfLostPacketsInOneMillisecondForRoute;
                lostSize = lostSizeForRoute;
                bestRoute = route;
            }
        });

        return [
            {'totalTrafficLoad': Math.round(totalTrafficLoad)},
            {'averagePacketsInBuffer': Math.round(averagePacketsInBuffer)},
            {'probabilityOfPacketLoss': probabilityOfPacketLoss.toFixed(UnsentPackets.decimalPlaces)},
            {'countOfLostPacketsInOneMillisecond': Math.round(countOfLostPacketsInOneMillisecond)},
            {'lostSize': lostSize},
            {'bestRoute': bestRoute},
            {'bestRoutePrettyView': InfoBlock.getPrettyViewOfBestRouteNames(bestRoute)}
        ];
    }
    //-----------------------------/public------------------------------//

    //------------------------------private------------------------------//

    //средняя интенсивность выходного потока
    static getAverageFlowRate(route) {

        let sumFlowRate = 0;
        for (let i = 0; i < route.length; i++) {
            let fromElemClass = BaseElem.getClassByDataId(BaseElem.getDataIdByNumber(route[i]));
            sumFlowRate += parseFloat(fromElemClass.getParam('averageFlowRate'));
        }

        return sumFlowRate / route.length;
    }

    //суммарная нагрузка трафика
    static getTotalTrafficLoad(route) { //пакетов
        let averageFlowRate = UnsentPackets.getAverageFlowRate(route);

        let commonPacketGenerationIntensity = 0; //суммарная интенсивность генерации пакетов
        route.forEach(function (elemNumber) {
            let elemClass = BaseElem.getClassByDataId(BaseElem.getDataIdByNumber(elemNumber));
            commonPacketGenerationIntensity += parseFloat(elemClass.getParam('packetGenerationIntensity'));
        });

        return commonPacketGenerationIntensity / parseFloat(averageFlowRate);
    }

    //среднее число пакетов в буфере
    static getAveragePacketsInBuffer(route, totalTrafficLoad) { //пакетов
        //выбираем самый маленький буфер
        let minBufferSize = 0;
        route.forEach(function (elemNumber) {
            let elemClass = BaseElem.getClassByDataId(BaseElem.getDataIdByNumber(elemNumber));
            let inputBufferSize = parseInt(elemClass.getParam('inputBufferSize'));
            if (inputBufferSize < minBufferSize || minBufferSize === 0) {
                minBufferSize = inputBufferSize;
            }
        });

        //считаем по формуле
        return minBufferSize / totalTrafficLoad;
    }

    //вероятность потери пакета
    static getProbabilityOfPacketLoss(totalTrafficLoad, averagePacketsInBuffer) { //безразмерная величина от 0 до 1
        return Math.pow(totalTrafficLoad, averagePacketsInBuffer) * (1 - totalTrafficLoad) / (1 - Math.pow(totalTrafficLoad, (averagePacketsInBuffer + 1)));
    }

    //число потерянных пакетов за одну миллисекунду
    static getCountOfLostPacketsInOneMillisecond(probabilityOfPacketLoss, route) { //пакетов/секунду
        let averageFlowRate = UnsentPackets.getAverageFlowRate(route[0]); //средняя интенсивность выходного потока по первому элементу цепи

        return probabilityOfPacketLoss * averageFlowRate * 1;
    }

    //размер потерянных данных
    static getLostSize(countOfLostPacketsInOneSecond, route) { //байт
        let fromElemClass = BaseElem.getClassByDataId(BaseElem.getDataIdByNumber(route[0]));
        let packetSize = fromElemClass.getParam('packetSize'); //размер передаваемых пакетов по первому элементу цепи

        return Math.round(packetSize * countOfLostPacketsInOneSecond);
    }
    //-----------------------------/private------------------------------//
}