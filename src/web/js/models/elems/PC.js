class PC extends BaseElem {
    //ReactionTime
    static minHardwareReactionTime = 0.030; //минимлаьное время обработки запроса железом (секунд)
    static maxHardwareReactionTime = 1; //максимальное время обработки запроса железом (секунд)

    //UnsentPackets
    static minIntervalBetweenTwoPackets = 0.001; //минимальный интервал между двумя соседними пакетами в секундах
    static maxIntervalBetweenTwoPackets = 0.050; //максимальный интервал между двумя соседними пакетами в секундах
    static minPacketGenerationIntensity = 1; //минимальная интенсивность генерации пакетов (пакетов в секунду)
    static maxPacketGenerationIntensity = 64; //максимальная интенсивность генерации пакетов (пакетов в секунду)
    static minPacketSize = 8; //минимальный размер передаваемого пакета (байт)
    static maxPacketSize = 128; //максимальный размер передаваемого пакета (байт)
    static minInputBufferSize = 128; //минимальный размер входного буфера (пакетов)
    static maxInputBufferSize = 512; //максимальный размер входного буфера (пакетов)

    //Performance
    static minSizeOfTransmittedInfo = 200; //минимальное количество передаваемых данных в секунду
    static maxSizeOfTransmittedInfo = 1000; //максимальное количество передаваемых данных в секунду

    constructor(elem, needSetPrams = true) {
        super(elem, needSetPrams);
        if(needSetPrams) {
            this.mapParams(elem);
        }
    }

    mapParams(elem) {

    }

    //ReactionTime
    getMaxHardwareReactionTime() {
        return PC.maxHardwareReactionTime;
    }

    getMinHardwareReactionTime() {
        return PC.minHardwareReactionTime;
    }

    //UnsentPackets
    getMinIntervalBetweenTwoPackets() {
        return PC.minIntervalBetweenTwoPackets;
    }

    getMaxIntervalBetweenTwoPackets() {
        return PC.maxIntervalBetweenTwoPackets;
    }

    getMinPacketGenerationIntensity() {
        return PC.minPacketGenerationIntensity;
    }

    getMaxPacketGenerationIntensity() {
        return PC.maxPacketGenerationIntensity;
    }

    getMinPacketSize() {
        return PC.minPacketSize;
    }

    getMaxPacketSize() {
        return PC.maxPacketSize;
    }

    getMinInputBufferSize() {
        return PC.minInputBufferSize;
    }

    getMaxInputBufferSize() {
        return PC.maxInputBufferSize;
    }

    getMinSizeOfTransmittedInfo() {
        return PC.minSizeOfTransmittedInfo;
    }

    getMaxSizeOfTransmittedInfo() {
        return PC.maxSizeOfTransmittedInfo;
    }
}
